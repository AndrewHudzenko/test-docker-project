package com.example.testdocker.controller;

import com.example.testdocker.dto.CarRequestDto;
import com.example.testdocker.dto.CarResponseDto;
import com.example.testdocker.model.Car;
import com.example.testdocker.service.CarService;
import com.example.testdocker.service.mapper.CarMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("demo")
public class DemoController {
    public final CarService carService;
    public final CarMapper carMapper;

    public DemoController(CarService carService, CarMapper carMapper) {
        this.carService = carService;
        this.carMapper = carMapper;
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello World!";
    }

    @GetMapping("/getCars")
    public List<CarResponseDto> getCars() {
        return carService.getAllCars().stream()
                .map(carMapper::toDto)
                .collect(Collectors.toList());
    }

    @PostMapping("/saveCar")
    public CarResponseDto saveCar(@RequestBody CarRequestDto carRequestDto) {
        Car car = carService.saveCar(carMapper.toModel(carRequestDto));
        return carMapper.toDto(car);
    }
}
