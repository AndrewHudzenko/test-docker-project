package com.example.testdocker.service;

import com.example.testdocker.model.Car;

import java.util.List;

public interface CarService {
    List<Car> getAllCars();

    Car saveCar(Car car);
}
