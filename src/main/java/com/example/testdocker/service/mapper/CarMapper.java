package com.example.testdocker.service.mapper;

import com.example.testdocker.dto.CarRequestDto;
import com.example.testdocker.dto.CarResponseDto;
import com.example.testdocker.model.Car;
import org.springframework.stereotype.Component;

@Component
public class CarMapper {
    public Car toModel(CarRequestDto carDto) {
        Car car = new Car();
        car.setMark(carDto.getMark());
        car.setColor(carDto.getColor());
        return car;
    }

    public CarResponseDto toDto(Car car) {
        CarResponseDto carResponseDto = new CarResponseDto();
        carResponseDto.setId(car.getId());
        carResponseDto.setMark(car.getMark());
        carResponseDto.setColor(car.getColor());
        return carResponseDto;
    }
}
